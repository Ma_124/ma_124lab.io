#!/bin/sh

if apk --version
then
    set -e
    apk add git
    apk add go
    apk add libsass sassc
    SASSC=sassc
elif npm --version
then
    set -e
    npm install sass
    SASSC=node_modules/sass/sass.js
else
    echo "Coudn't detect package manager"
    exit 1
fi

set -e

eval $SASSC style/style.scss public/style.css
go get github.com/benbjohnson/ego

cp CNAME public/CNAME
cp LICENSE public/license.txt
cp _redirects public/_redirects

go get github.com/aws/aws-lambda-go/...

cd funcs-src
rm -f l/license_list_gen.go
for f in ../licenses/*; do
    go run ../gen_license.go $f
done
echo "}" >> l/license_list_gen.go
cat l/license_list_gen.go

for f in *; do
    go build -o "../funcs/$f" $f/*.go
done
cd ..

go run gen.go

