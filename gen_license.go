// +build ignore

package main

// To see the license for the Html and Css go to https://rem.mit-license.org/

import (
	"html"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	data, err := ioutil.ReadFile(os.Args[1])
	os.Args[1] = os.Args[1][12 : len(os.Args[1])-4]
	if err != nil {
		panic(err)
	}

	f, err := os.Create("l/" + strings.ToLower(os.Args[1]) + "_gen.go")
	if err != nil {
		panic(err)
	}

	_, err = f.Write([]byte(`package main

func gen` + strings.Title(strings.ToLower(os.Args[1])) + `Txt(year, author, mail, site string) string {
	return "Copyright " + year + " " + author + addIfNotEmpty(", ", site, "") + addIfNotEmpty(" <", mail, ">") + "\n\n" + ` + "`" + string(data) + "`" + `
}

func gen` + strings.Title(strings.ToLower(os.Args[1])) + `DuskHtml(year, author, mail, site string) string {
	return ` + "`" + strings.Replace(strings.Replace(strings.Replace(`<html id="home" lang="en"><head><style>@import url(https://fonts.googleapis.com/css?family=Open+Sans:300,400);::selection{background:#de5833;color:#eff7ff}::-moz-selection{background:#de5833;color:#eff7ff}*{margin:0;padding:0}body{font-family:'Open Sans',Helvetica Neue,Helvetica,Arial,sans-serif;font-size:1.1em;background:#272b35;color:#67778a;margin:86px 0;-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased}a:hover,a:link,a:visited{position:relative;text-decoration:none;color:#de5833;-webkit-transition:color .3s ease-in-out;transition:color .3s ease-in-out;-webkit-transform:scaleX(0);transform:scaleX(0)}a:hover,article h1{color:#eff7ff}a:before{content:" ";position:absolute;width:100%;height:2px;bottom:0;left:0;background-color:#de5833;visibility:hidden;-webkit-transform:scaleX(0);transform:scaleX(0);-webkit-transition:all .3s ease-in-out;transition:all .3s ease-in-out}article h1,article h1+p{background:#1F222A;text-align:center}a:hover:before{visibility:visible;-webkit-transform:scaleX(1);transform:scaleX(1)}article{display:block;position:relative;margin:0 auto}article h1{margin:0;padding:60px .4em 0;font-size:2em;font-weight:300}article h1+p{max-width:100%;margin-top:0;padding-top:.1em;padding-bottom:60px;font-size:1em}article p,footer p{padding:0 1.4em}article h1+p+p{margin-top:1.6em}article p{text-align:justify;line-height:1.5;max-width:960px;margin:1em auto}article p:last-child{padding-bottom:1.8em;font-size:.9em}footer{margin:0 auto;font-size:.8em;text-align:center}#gravatar{display:block;position:absolute;top:-43px;left:50%;margin-left:-43px;border-radius:100%;border:3px solid #272b35}@media (max-width:750px){article h1+p{font-size:.8em}}</style><title>{.LicenseName}</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=0.7"><script>document.createElement('article');document.createElement('footer');</script></head><body><article><img id="gravatar" src="https://www.gravatar.com/avatar/`+"` + gravatar(mail) + `?d=robohash"+
		`"><h1>{.LicenseName}</h1><p>{.CopyrightNotice}</p>{.LicenseText}</article><footer><p><a href="https://ma124.js.org/misc/licenses_about.html">Create your own license that you can always link to.</a></p></footer></body></html>`+"`"+`
}
`, "{.LicenseName}", os.Args[1], -1), "{.LicenseText}", "<p>"+strings.Replace(html.EscapeString(string(data)), "\n\n", "</p><p>", -1)+"</p>", -1), "{.CopyrightNotice}", "` + "+`"Copyright " + year + " " + author + addIfNotEmpty(", <a href='https://" + site + "'>", site, "</a>") + addIfNotEmpty(" &lt;<a href='mailto://" + mail + "'>", mail, "</a>&gt;") + `+"`", -1)))
	if err != nil {
		panic(err)
	}
	f.Close()

	if _, err := os.Stat("l/license_list_gen.go"); err != nil {
		f, err = os.Create("l/license_list_gen.go")
		if err != nil {
			panic(err)
		}

		_, err = f.Write([]byte("package main\n\nvar LicenseGenerators = map[string]LicenseGenerator {\n"))
		if err != nil {
			panic(err)
		}
	} else {
		f, err = os.OpenFile("l/license_list_gen.go", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			panic(err)
		}
	}
	defer f.Close()

	_, err = f.Write([]byte(`"` + strings.ToLower(os.Args[1]) + `.txt": gen` + strings.Title(strings.ToLower(os.Args[1])) + `Txt, "` + strings.ToLower(os.Args[1]) + `.html": gen` + strings.Title(strings.ToLower(os.Args[1])) + "DuskHtml,\n"))
	if err != nil {
		panic(err)
	}
}
