package main

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type LicenseGenerator func(year, author, mail, site string) string

func main() {
	lambda.Start(Handler)
}

// Handles /l/** and /.netlify/functions/l/** requests
// URL Schema:	/l/<LICENSE>/<NAME>~<SITE>~<EMAIL>/<YEAR>
// So allowed are:
// /l/<LICENSE>/<NAME>
// /l/<LICENSE>/<NAME>~<SITE>
// /l/<LICENSE>/<NAME>~<SITE>~<MAIL>
// /l/<LICENSE>/<NAME>~~<MAIL>
//
// /l/<LICENSE>/<NAME>/<YEAR>
// /l/<LICENSE>/<NAME>~<SITE>/<YEAR>
// /l/<LICENSE>/<NAME>~<SITE>~<MAIL>/<YEAR>
// /l/<LICENSE>/<NAME>~~<MAIL>/<YEAR>
//
// And each <YEAR> can be:
//  2016		-> 2016-2019 (current year)
// @2016		-> 2016
//  2016-2017	-> 2016-2017
func Handler(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	var license string
	var name string
	var mail string
	var site string
	var year string
	var basePathIndex int

	if req.Path[0] == '/' {
		req.Path = req.Path[1:]
	}
	if req.Path[len(req.Path)-1] == '/' {
		req.Path = req.Path[:len(req.Path)-1]
	}
	path := strings.Split(req.Path, "/")
	if path[0] == "l" {
		basePathIndex = 0
	} else if path[0] == ".netlify" {
		basePathIndex = 2
	} else {
		return &events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       `{ "error": "calling from unknown URL", "error-args": ["` + req.Path + `"] }`,
		}, nil
	}

	if len(path) > basePathIndex+1 {
		license = strings.ToLower(path[basePathIndex+1])
		if !strings.HasSuffix(license, ".txt") && !strings.HasSuffix(license, ".html") {
			license += ".html"
		}

	} else {
		return &events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       `{ "error": "no license name specified" }`,
		}, nil
	}

	if len(path) > basePathIndex+2 {
		nes := strings.Split(path[basePathIndex+2], "~")
		name = nes[0]
		if len(nes) > 1 {
			site = nes[1]
		}
		if len(nes) > 2 {
			mail = nes[2]
		}

		if name == "" {
			name = "Ma_124"
			mail = "ma_124@outlook.com"
			site = "ma124.js.org"
		}
	} else {
		name = "Ma_124"
		mail = "ma_124@outlook.com"
		site = "ma124.js.org"
	}

	if len(path) > basePathIndex+3 {
		year = path[basePathIndex+3]
		if year[0] == '@' {
			year = year[1:]
		} else if !strings.Contains(year, "-") {
			y := strconv.Itoa(time.Now().Year())
			if year != y {
				year += "-" + y
			}
		}
	} else {
		year = strconv.Itoa(time.Now().Year())
	}

	lg := LicenseGenerators[license]
	if lg == nil {
		return &events.APIGatewayProxyResponse{
			StatusCode: 404,
			Body:       `{ "error": "unknown license name specified", "error-args": ["` + license + `"] }`,
		}, nil
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       lg(year, name, mail, site),
	}, nil
}

func sanitizePath(s string) string {
	b := make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		c := s[i]
		if c >= 'A' && c <= 'Z' {
			c += 'a' - 'A'
		} else if c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '-' || c == '_' {

		} else {
			return ""
		}
		b[i] = c
	}
	return string(b)
}

func gravatar(mail string) string {
	sum := md5.Sum(bytes.ToLower([]byte(bytes.TrimSpace([]byte(mail)))))
	return hex.EncodeToString(sum[:])
}

func addIfNotEmpty(pre, cont, suf string) string {
	if len(cont) != 0 {
		return pre + cont + suf
	}
	return ""
}
