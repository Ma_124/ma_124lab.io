package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(Handler)
}

func Handler(req events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	pkgName := ""

	return &events.APIGatewayProxyResponse{
		StatusCode: 404,
		Body:       `<!DOCTYPE html><html><head>` +
					`<meta http-equiv="content-type" content="text/html; charset=utf-8">` +
					`<meta name="go-import" content="ma124.js.org/gopkg/` + pkgName + `/` + `">` +
					`</head><body></body></html>`,
	}, nil
}
