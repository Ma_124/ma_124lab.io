package main

import (
	"os"
	"text/template"
)

type LsFile struct {
	Name             string
	Day, Month, Year string
	Href             string
}

func main() {
	//compFile("index")
	//return
	err := compileTemplate(struct {
		Projects  []LsFile
		BlogPosts []LsFile
	}{
		Projects: []LsFile{
			{"Fit", "08", "Jul", "2018", "https://gitlab.com/Ma_124/fit"},
			{"DotFiles", "10", "Sep", "2018", "https://gitlab.com/Ma_124/dotfiles"},
			{"Page", "14", "Oct", "2018", "https://gitlab.com/Ma_124/ma_124.gitlab.io"},
			{"Licenses", "10", "Apr", "2018", "https://github.com/Ma124/Licenses"},
			{"BeSafe!", "25", "Mar", "2018", "https://github.com/Ma124/BeSafe"},
			{"License API", "15", "Dec", "2018", "/misc/licenses_about.html"},
			{"Misc Sites", "22", "Dec", "2018", "/misc"},
		},
	}, "index.html", "ls.html", "cmd.html", "html-link.html")

	if err != nil {
		panic(err)
	}
}

func compileTemplate(data interface{}, files ...string) error {
	ffile := files[0]
	for i := range files {
		files[i] = "template/" + files[i]
	}

	tmpl, err := template.New(ffile).ParseFiles(files...)
	if err != nil {
		return err
	}

	if data != nil {
		f, err := os.Create("public/" + ffile)
		if err != nil {
			return err
		}
		defer f.Close()

		err = tmpl.Execute(f, data)
		if err != nil {
			return err
		}
	}

	return nil
}

//func compFile(file string) {
//	templ, err := ego.ParseFile("template/" + file + ".ego.go")
//	if err != nil {
//		panic(err)
//	}
//
//	f, err := os.Create(file + "_gen.go")
//	if err != nil {
//		panic(err)
//	}
//
//	_, err = templ.WriteTo(f)
//	if err != nil {
//		panic(err)
//	}
//
//	err = f.Close()
//	if err != nil {
//		panic(err)
//	}
//
//	cmd := exec.Command("go", "run", file+"_gen.go")
//	cmd.Stdout = os.Stdout
//	cmd.Stderr = os.Stderr
//	cmd.Stdin = os.Stdin
//
//	err = cmd.Run()
//	if err != nil {
//		panic(err)
//	}
//}
//
//func compileTemplate(data interface{}, files ...string) error {
//	ffile := files[0]
//	for i := range files {
//		files[i] = "template/" + files[i]
//	}
//
//	tmpl, err := template.New(ffile).ParseFiles(files...)
//	if err != nil {
//		return err
//	}
//
//	if data != nil {
//		f, err := os.Create("public/" + ffile)
//		if err != nil {
//			return err
//		}
//		defer f.Close()
//
//		err = tmpl.Execute(f, data)
//		if err != nil {
//			return err
//		}
//	}
//
//	return nil
//}
